import 'package:bloc/bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/AuthenticationBlocEvent.dart';
import 'package:maper_app/bloc/AuthenticationBloc/AuthenticationBlocState.dart';
import 'package:maper_app/repository/ApiRepository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationBlocEvent, AuthenticationBlocState> {
  final ApiRepository apiRepository;

  AuthenticationBloc({required this.apiRepository})
      : super(AuthenticationBlocStateLoggedOut()) {
    on<AuthenticationBlocEventLogin>((event, emit) async {
      bool hasToken = await apiRepository.hasToken();
      if (hasToken) {
        emit(AuthenticationBlocStateLoggedIn());
      } else {
        emit(AuthenticationBlocStateLoggedOut());
      }
    });
    on<AuthenticationBlocEventLogout>((event, emit) async {
      try {
        await apiRepository.logout();
        emit(AuthenticationBlocStateLoggedOut());
      } catch (exception) {
        emit(AuthenticationBlocStateLoggedOut());
      }
    });
  }
}

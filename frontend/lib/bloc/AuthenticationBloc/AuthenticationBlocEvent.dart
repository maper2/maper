import 'package:equatable/equatable.dart';

class AuthenticationBlocEvent extends Equatable {
  const AuthenticationBlocEvent();

  @override
  List<Object> get props => [];
}

class AuthenticationBlocEventLogin extends AuthenticationBlocEvent {
  final String? url;
  final String? username;
  final String? password;

  const AuthenticationBlocEventLogin({this.url, this.username, this.password});

  @override
  List<Object> get props => [url ?? "", username ?? "", password ?? ""];
}

class AuthenticationBlocEventLogout extends AuthenticationBlocEvent {}

import 'package:equatable/equatable.dart';

abstract class AuthenticationBlocState extends Equatable {
  const AuthenticationBlocState();

  @override
  List<Object> get props => [];
}

class AuthenticationBlocStateLoggedIn extends AuthenticationBlocState {}

class AuthenticationBlocStateLoggedOut extends AuthenticationBlocState {}

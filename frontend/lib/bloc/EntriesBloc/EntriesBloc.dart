import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesState.dart';
import 'package:maper_app/domain/entry.dart';
import 'package:maper_app/repository/ApiRepository.dart';

class EntriesBloc extends Bloc<EntriesEvent, EntriesState> {
  final ApiRepository apiRepository;

  EntriesBloc({required this.apiRepository}) : super(EntriesStateInitial()) {
    on<EntriesEventRefresh>((event, emit) async {
      emit(EntriesStateLoading());
      try {
        List<Entry> entries = await apiRepository.getEntries();
        emit(EntriesStateData(entries: entries));
      } catch (e) {
        emit(EntriesStateError(error: e.toString()));
      }
    });
    on<EntriesEventCreateEntry>((event, emit) async {
      emit(EntriesStateLoading());
      try {
        await apiRepository.createNewEntry(event.newEntry);
        List<Entry> entries = await apiRepository.getEntries();
        emit(EntriesStateData(entries: entries));
      } catch (e) {
        emit(EntriesStateError(error: e.toString()));
      }
    });
    on<EntriesEventSaveEntry>((event, emit) async {
      emit(EntriesStateLoading());
      try {
        await apiRepository.saveEntry(event.entry);
        List<Entry> entries = await apiRepository.getEntries();
        emit(EntriesStateData(entries: entries));
      } catch (e) {
        emit(EntriesStateError(error: e.toString()));
      }
    });
    on<EntriesEventDeleteEntry>((event, emit) async {
      emit(EntriesStateLoading());
      try {
        await apiRepository.deleteEntry(event.entry);
        List<Entry> entries = await apiRepository.getEntries();
        emit(EntriesStateData(entries: entries));
      } catch (e) {
        emit(EntriesStateError(error: e.toString()));
      }
    });
  }
}

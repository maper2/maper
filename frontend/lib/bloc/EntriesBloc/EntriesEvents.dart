import 'package:equatable/equatable.dart';
import 'package:maper_app/domain/entry.dart';

class EntriesEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class EntriesEventRefresh extends EntriesEvent {}

class EntriesEventCreateEntry extends EntriesEvent {
  final NewEntry newEntry;
  EntriesEventCreateEntry({required this.newEntry});
}

class EntriesEventSaveEntry extends EntriesEvent {
  final Entry entry;
  EntriesEventSaveEntry({required this.entry});
}

class EntriesEventDeleteEntry extends EntriesEvent {
  final Entry entry;
  EntriesEventDeleteEntry({required this.entry});
}

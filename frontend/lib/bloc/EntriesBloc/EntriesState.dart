import 'package:equatable/equatable.dart';
import 'package:maper_app/domain/entry.dart';

class EntriesState extends Equatable{
  @override
  List<Object?> get props =>[];
}


class EntriesStateInitial extends EntriesState{}
class EntriesStateLoading extends EntriesState{}

class EntriesStateData extends EntriesState{
  final List<Entry> entries;

  EntriesStateData({required this.entries});
}

class EntriesStateError extends EntriesState{
  final String error;

  EntriesStateError({required this.error});
}


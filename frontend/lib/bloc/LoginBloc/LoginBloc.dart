import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/bloc.dart';
import 'package:maper_app/bloc/LoginBloc/LoginEvent.dart';
import 'package:maper_app/bloc/LoginBloc/LoginState.dart';
import 'package:maper_app/repository/ApiRepository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final ApiRepository apiRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    required this.apiRepository,
    required this.authenticationBloc,
  }) : super(LoginStateInitial()) {
    on<LoginButtonPressed>((event, emit) async {
      emit(LoginStateInProgress());
      try {
        await apiRepository.login(
            url: event.url, username: event.username, password: event.password);
        authenticationBloc.add(const AuthenticationBlocEventLogin());
        emit(LoginStateDone());
        add(LoginLoadInitialData());
      } catch (e) {
        emit(LoginStateFailure(error: e.toString()));
        add(LoginLoadInitialData());
      }
    });
    on<LoginRegisterButtonPressed>((event, emit) async {
      emit(LoginStateInProgress());
      try {
        await apiRepository.register(
            url: event.url, username: event.username, password: event.password);

        add(LoginLoadInitialData());
      } catch (e) {
        emit(LoginStateFailure(error: e.toString()));
        add(LoginLoadInitialData());
      }
    });
    on<LoginLoadInitialData>((event, emit) async {
      String? url = await apiRepository.getPresavedUrl();
      String? username = await apiRepository.getPresavedUsername();

      emit(LoginStateInitialData(
          preSavedUsername: username ?? "", preSavedUrl: url ?? ""));
    });
  }
}

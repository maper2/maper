import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class LoginLoadInitialData extends LoginEvent {}

class LoginButtonPressed extends LoginEvent {
  final String url;
  final String username;
  final String password;

  const LoginButtonPressed({
    required this.url,
    required this.username,
    required this.password,
  });

  @override
  List<Object> get props => [url, username, password];

  @override
  String toString() => 'LoginButtonPressed {url: $url, username: $username }';
}

class LoginRegisterButtonPressed extends LoginEvent {
  final String url;
  final String username;
  final String password;

  const LoginRegisterButtonPressed({
    required this.url,
    required this.username,
    required this.password,
  });

  @override
  List<Object> get props => [url, username, password];

  @override
  String toString() =>
      'LoginRegisterButtonPressed {url: $url, username: $username }';
}


import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}
class LoginStateInitial extends LoginState {}

class LoginStateInitialData extends LoginState {
  final String preSavedUsername;
  final String preSavedUrl;

  const LoginStateInitialData({
    required this.preSavedUsername,
    required this.preSavedUrl,
  });

  @override
  List<Object> get props => [preSavedUrl, preSavedUsername];
  @override
  String toString() => 'LoginStateInitialData { url: $preSavedUrl, username: $preSavedUsername }';

}

class LoginStateInProgress extends LoginState {}
class LoginStateDone extends LoginState {}

class LoginStateFailure extends LoginState {
  final String error;

  const LoginStateFailure({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginStateFailure { error: $error }';
}

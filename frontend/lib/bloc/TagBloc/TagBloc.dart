import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/AuthenticationBloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/AuthenticationBlocEvent.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/TagBloc/TagEvent.dart';
import 'package:maper_app/bloc/TagBloc/TagState.dart';
import 'package:maper_app/domain/tag.dart';
import 'package:maper_app/repository/ApiRepository.dart';
import 'package:maper_app/repository/exceptions/SessionExpiredException.dart';

class TagBloc extends Bloc<TagEvent, TagState> {
  final ApiRepository apiRepository;
  final AuthenticationBloc authenticationBloc;
  final EntriesBloc entriesBloc;

  TagBloc(
      {required this.apiRepository,
      required this.authenticationBloc,
      required this.entriesBloc})
      : super(TagStateInitial()) {
    on<TagEventRefresh>((event, emit) async {
      emit(TagStateLoading());
      try {
        List<Tag> tags = await apiRepository.getTags();
        emit(TagStateData(tags: tags));
      } on SessionExpiredException {
        //print("Session Expired exception");

        emit(TagStateInitial());
        authenticationBloc.add(AuthenticationBlocEventLogout());
      } catch (e) {
        //print("general exception");

        emit(TagStateError(error: e.toString()));
      }
    });
    on<TagEventCreateTag>((event, emit) async {
      emit(TagStateLoading());
      try {
        await apiRepository.createNewTag(event.newTag);
        List<Tag> tags = await apiRepository.getTags();
        emit(TagStateData(tags: tags));
      } catch (e) {
        emit(TagStateError(error: e.toString()));
      }
    });
    on<TagEventSaveTag>((event, emit) async {
      emit(TagStateLoading());
      try {
        await apiRepository.saveTag(event.tag);
        List<Tag> tags = await apiRepository.getTags();
        emit(TagStateData(tags: tags));
      } catch (e) {
        emit(TagStateError(error: e.toString()));
      }
    });
    on<TagEventDeleteTag>((event, emit) async {
      emit(TagStateLoading());
      try {
        await apiRepository.deleteTag(event.tag);
        entriesBloc.add(EntriesEventRefresh());
        List<Tag> tags = await apiRepository.getTags();
        emit(TagStateData(tags: tags));
      } catch (e) {
        emit(TagStateError(error: e.toString()));
      }
    });
  }
}

import 'package:equatable/equatable.dart';
import 'package:maper_app/domain/tag.dart';

class TagEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class TagEventRefresh extends TagEvent {}

class TagEventCreateTag extends TagEvent {
  final NewTag newTag;
  TagEventCreateTag({required this.newTag});
}

class TagEventSaveTag extends TagEvent {
  final Tag tag;
  TagEventSaveTag({required this.tag});
}

class TagEventDeleteTag extends TagEvent {
  final Tag tag;
  TagEventDeleteTag({required this.tag});
}

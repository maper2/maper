import 'package:equatable/equatable.dart';
import 'package:maper_app/domain/tag.dart';

class TagState extends Equatable{
  @override
  List<Object?> get props => [];
}

class TagStateInitial extends TagState{}
class TagStateLoading extends TagState{}

class TagStateData extends TagState{
  final List<Tag> tags;

  TagStateData({required this.tags});
  @override
  List<Object?> get props => [tags];
}

class TagStateError extends TagState{
  final String error;

  TagStateError({required this.error});
  @override
  List<Object?> get props => [error];
}

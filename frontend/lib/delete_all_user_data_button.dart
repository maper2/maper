import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/bloc.dart';
import 'package:maper_app/repository/ApiRepository.dart';

class DeleteAllUserDataButton extends StatelessWidget {
  const DeleteAllUserDataButton({super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: () {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: const Text("Do you want to delete all your data?"),
            action: SnackBarAction(
                label: "delete all my data!",
                onPressed: () {
                  try {
                    context
                        .read<ApiRepository>()
                        .delteAllUserData()
                        .then((value) {
                      if (value) {
                        context
                            .read<AuthenticationBloc>()
                            .add(AuthenticationBlocEventLogout());
                        Navigator.of(context).pop();
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text(
                                'Your data was successfully deleted. Farewell')));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Deletion returned false...')));
                      }
                    });
                  } catch (e) {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Your data could not be delted! :(')));
                  }
                }),
          ),
        );
      },
      icon: const Icon(Icons.delete_forever),
      label: const Text("Delete all my data"),
    );
  }
}

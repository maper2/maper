import 'package:maper_app/domain/tag.dart';

class Entry {
  String id;
  String name;
  String url;
  String? notes;
  List<Tag> tags;

  Entry(
      {required this.id,
      required this.name,
      required this.url,
      this.notes,
      required this.tags});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Entry && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  factory Entry.fromMap(Map<String, dynamic> data) => Entry(
      id: data["id"],
      name: data["name"],
      url: data["url"],
      notes: data["notes"],
      tags: data["tags"].map((e) => Tag.fromMap(e)).toList().cast<Tag>());

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "url": url,
        "notes": notes,
        "tags": tags.map((tag) => tag.toMap()).toList(),
      };
}

// used for creating new entries, id is specified by the backend
class NewEntry {
  String name;
  String url;
  String? notes;
  List<Tag> tags;

  NewEntry(
      {required this.name, required this.url, this.notes, required this.tags});

  Map<String, dynamic> toMap() => {
        "name": name,
        "url": url,
        "notes": notes,
        "tag_ids": tags.map((t) => t.id).toList(),
      };
}

class Tag {
  String id;
  String name;

  Tag({
    required this.id,
    required this.name,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Tag && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  factory Tag.fromMap(Map<String, dynamic> data) => Tag(
        id: data["id"],
        name: data["name"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
      };

  @override
  String toString() {
    return name;
  }
}

// used for creating new entries, id is specified by the backend
class NewTag {
  String name;

  NewTag({
    required this.name,
  });

  Map<String, dynamic> toMap() => {
        "name": name,
      };
}

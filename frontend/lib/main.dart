import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/LoginBloc/LoginBloc.dart';
import 'package:maper_app/bloc/LoginBloc/LoginEvent.dart';
import 'package:maper_app/bloc/TagBloc/bloc.dart';
import 'package:maper_app/pages/home_page_mobile.dart';
import 'package:maper_app/pages/home_page_tablet.dart';
import 'package:maper_app/repository/ApiRepository.dart';
import 'package:http/http.dart' as http;

void main() {
  final http.Client httpClient = http.Client();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider<ApiRepository>(
            create: (context) => ApiRepository(httpClient: httpClient)),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
            create: (context) => AuthenticationBloc(
              apiRepository: context.read<ApiRepository>(),
            )..add(
                const AuthenticationBlocEventLogin(),
              ),
          ),
          BlocProvider<LoginBloc>(
            create: (context) => LoginBloc(
              apiRepository: context.read<ApiRepository>(),
              authenticationBloc: context.read<AuthenticationBloc>(),
            )..add(
                LoginLoadInitialData(),
              ),
          ),
          BlocProvider<EntriesBloc>(
            create: (context) => EntriesBloc(
              apiRepository: context.read<ApiRepository>(),
            )..add(
                EntriesEventRefresh(),
              ),
          ),
          BlocProvider<TagBloc>(
            create: (context) => TagBloc(
              apiRepository: context.read<ApiRepository>(),
              authenticationBloc: context.read<AuthenticationBloc>(),
              entriesBloc: context.read<EntriesBloc>(),
            )..add(
                TagEventRefresh(),
              ),
          ),
        ],
        child: const MaperApp(),
      ),
    ),
  );
}

class MaperApp extends StatelessWidget {
  const MaperApp({super.key});

  bool _isMediumScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > 640.0;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Maper',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: const Color.fromRGBO(7, 89, 133, 1)), // #075985
        useMaterial3: true,
      ),
      home: _isMediumScreen(context)
          ? const HomePageTablet()
          : const HomePageMobile(),
    );
  }
}

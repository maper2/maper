import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/AuthenticationBloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/AuthenticationBlocEvent.dart';
import 'package:maper_app/bloc/LoginBloc/bloc.dart';
import 'package:maper_app/pages/profile_page.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.inversePrimary,
            ),
            child: const Text("Menu"),
          ),
          ListTile(
            title: const Text('Profile'),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const ProfilePage(),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('Logout'),
            onTap: () {
              context
                  .read<AuthenticationBloc>()
                  .add(AuthenticationBlocEventLogout());
              context.read<LoginBloc>().add(LoginLoadInitialData());
            },
          ),
        ],
      ),
    );
  }
}

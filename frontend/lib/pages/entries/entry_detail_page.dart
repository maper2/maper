import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/domain/entry.dart';
import 'package:maper_app/pages/entries/entry_edit_page.dart';
import 'package:url_launcher/url_launcher.dart';

class EntryDetailPage extends StatelessWidget {
  final Entry entry;

  const EntryDetailPage({super.key, required this.entry});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(
          "Entry ${entry.name}",
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            child: Wrap(
              spacing: 8.0, // gap between adjacent chips
              runSpacing: 4.0, // gap between lines
              children: [
                Text(
                  entry.name,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20),
                ),
                OutlinedButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => EntryEditPage(entry: entry),
                      ),
                    );
                  },
                  label: const Text("edit"),
                  icon: const Icon(Icons.edit),
                ),
                OutlinedButton.icon(
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                        'Do you want to delete Entry ${entry.name}?',
                      ),
                      action: SnackBarAction(
                        label: 'Delete',
                        onPressed: () {
                          context.read<EntriesBloc>().add(
                                EntriesEventDeleteEntry(
                                  entry: entry,
                                ),
                              );
                          Navigator.pop(context);
                        },
                      ),
                    ));
                  },
                  label: const Text("delete"),
                  icon: const Icon(Icons.delete),
                ),
              ],
            ),
          ),
          Container(
            height: 50,
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: entry.tags.length,
              separatorBuilder: (context, index) {
                return const SizedBox(width: 10);
              },
              itemBuilder: (context, index) {
                return Chip(label: Text(entry.tags[index].name));
              },
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            child: Row(
              children: <Widget>[
                IconButton(
                  icon: const Icon(Icons.open_in_browser),
                  onPressed: () async {
                    await launchUrl(
                      Uri.parse(entry.url),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: Text(entry.url),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            child: Text(entry.notes ?? ""),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/TagBloc/TagBloc.dart';
import 'package:maper_app/bloc/TagBloc/TagState.dart';
import 'package:maper_app/domain/entry.dart';
import 'package:maper_app/domain/tag.dart';
import 'package:multiselect/multiselect.dart';

class EntryEditPage extends StatefulWidget {
  Entry entry;

  EntryEditPage({super.key, required this.entry});

  @override
  State<EntryEditPage> createState() => _EntryEditPageState(entry: entry);
}

class _EntryEditPageState extends State<EntryEditPage> {
  Entry entry;

  _EntryEditPageState({required this.entry});
  final _formKey = GlobalKey<FormState>();

  final _nameTextEditingController = TextEditingController();
  final _notesTextEditingController = TextEditingController();
  final _urlTextEditingController = TextEditingController();

  List<Tag> _selectedTags = [];

  _handleSubmit() {
    if (_formKey.currentState!.validate()) {
      entry.name = _nameTextEditingController.text;
      entry.url = _urlTextEditingController.text;
      entry.notes = _notesTextEditingController.text;
      entry.tags = _selectedTags;
      context.read<EntriesBloc>().add(
            EntriesEventSaveEntry(
              entry: entry,
            ),
          );
      Navigator.pop(context);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    _nameTextEditingController.text = entry.name;
    _notesTextEditingController.text = entry.notes ?? "";
    _urlTextEditingController.text = entry.url;
    setState(() {
      _selectedTags = entry.tags;
    });

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Edit Entry"),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _handleSubmit,
        label: const Text("Save"),
        icon: const Icon(Icons.save),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                autocorrect: false,
                controller: _nameTextEditingController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Name',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                autocorrect: false,
                controller: _urlTextEditingController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'URL',
                ),
                validator: (value) {
                  if (!Uri.parse(value ?? "").isAbsolute ||
                      (value ?? "").isEmpty) {
                    return 'Please enter a valid url';
                  }
                  return null;
                },
              ),
            ),
            BlocBuilder<TagBloc, TagState>(
              builder: (context, state) {
                if (state is TagStateData) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: DropDownMultiSelect(
                      isDense: true,
                      options: state.tags.map<Tag>((tag) => tag).toList(),
                      selectedValues: _selectedTags,
                      whenEmpty: 'Tags',
                      onChanged: (tags) {
                        setState(() {
                          _selectedTags = tags;
                        });
                      },
                    ),
                  );
                } else {
                  return Container();
                }
              },
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                maxLines: 8,
                autocorrect: false,
                controller: _notesTextEditingController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Notes',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  FilledButton(
                    onPressed: _handleSubmit,
                    child: const Text("Submit"),
                  ),
                  OutlinedButton.icon(
                    onPressed: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          'Do you want to delete Entry ${entry.name}?',
                        ),
                        action: SnackBarAction(
                          label: 'Delete',
                          onPressed: () {
                            context.read<EntriesBloc>().add(
                                  EntriesEventDeleteEntry(
                                    entry: entry,
                                  ),
                                );
                            Navigator.pop(context);
                          },
                        ),
                      ));
                    },
                    label: const Text("delete"),
                    icon: const Icon(Icons.delete),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

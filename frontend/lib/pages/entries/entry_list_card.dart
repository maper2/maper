import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/domain/entry.dart';
import 'package:maper_app/pages/entries/entry_detail_page.dart';
import 'package:url_launcher/url_launcher.dart';

class EntryListCard extends StatelessWidget {
  final Entry entry;

  const EntryListCard({super.key, required this.entry});

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.hardEdge,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => EntryDetailPage(entry: entry),
            ),
          );
        },
        onLongPress: () {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              'Do you want to delete Entry ${entry.name}?',
            ),
            action: SnackBarAction(
              label: 'Delete',
              onPressed: () {
                context.read<EntriesBloc>().add(
                      EntriesEventDeleteEntry(
                        entry: entry,
                      ),
                    );
              },
            ),
          ));
        },
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(0),
            child: ListTile(
                contentPadding: const EdgeInsets.all(2),
                horizontalTitleGap: 10,
                dense: false,
                title: Text(
                  entry.name,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text(
                  entry.notes ?? "",
                  maxLines: 1,
                  //entry.tags.map((t) => t.name).join(", "),
                  overflow: TextOverflow.ellipsis,
                ),
                leading: IconButton(
                  icon: const Icon(Icons.open_in_browser),
                  onPressed: () async {
                    await launchUrl(
                      Uri.parse(entry.url),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                ),
                trailing: Column(
                  children: [
                    const Icon(Icons.label_outline),
                    Text("${entry.tags.length}")
                  ],
                )),
          ),
        ),
      ),
    );
  }
}

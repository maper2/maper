import 'package:debounce_throttle/debounce_throttle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesState.dart';
import 'package:maper_app/bloc/TagBloc/TagBloc.dart';
import 'package:maper_app/bloc/TagBloc/TagState.dart';
import 'package:maper_app/domain/tag.dart';
import 'package:maper_app/pages/entries/entry_create_page.dart';
import 'package:maper_app/pages/entries/entry_list_card.dart';
import 'package:multiselect/multiselect.dart';

class EntryListPage extends StatefulWidget {
  const EntryListPage({super.key});
  @override
  State<StatefulWidget> createState() {
    return _EntryListPageState();
  }
}

class _EntryListPageState extends State<EntryListPage> {
  final _searchTextEditingController = TextEditingController();
  List<Tag> _selectedTags = [];
  final debouncer =
      Debouncer<String>(const Duration(milliseconds: 300), initialValue: "");

  bool _isMediumScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > 640.0;
  }

  @override
  Widget build(BuildContext context) {
    _searchTextEditingController
        .addListener(() => debouncer.value = _searchTextEditingController.text);
    debouncer.values.listen((search) => setState(() => {}));

    Widget smallScreenFilterWidgets() {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(5),
            child: TextFormField(
              expands: false,
              autocorrect: false,
              controller: _searchTextEditingController,
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Search',
              ),
            ),
          ),
          BlocBuilder<TagBloc, TagState>(
            builder: (context, state) {
              if (state is TagStateData) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: DropDownMultiSelect(
                        isDense: true,
                        options: state.tags.map<Tag>((tag) => tag).toList(),
                        selectedValues: _selectedTags,
                        whenEmpty: 'Tags',
                        onChanged: (tags) {
                          setState(() {
                            _selectedTags = tags;
                          });
                        },
                      ),
                    ),
                  ],
                );
              } else {
                return Container();
              }
            },
          ),
        ],
      );
    }

    Widget largerScreenFilterWidgets() {
      return Row(
        children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: TextFormField(
                expands: false,
                autocorrect: false,
                controller: _searchTextEditingController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Search',
                ),
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: BlocBuilder<TagBloc, TagState>(
              builder: (context, state) {
                if (state is TagStateData) {
                  return Column(children: [
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: DropDownMultiSelect(
                        options: state.tags.map<Tag>((tag) => tag).toList(),
                        selectedValues: _selectedTags,
                        whenEmpty: 'Tags',
                        onChanged: (tags) {
                          setState(() {
                            _selectedTags = tags;
                          });
                        },
                      ),
                    ),
                  ]);
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      );
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => const EntryCreatePage(),
              ),
            );
          }),
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 8.0, // gap between adjacent chips
            runSpacing: 4.0, // gap between lines
            children: [
              OutlinedButton.icon(
                onPressed: () {
                  context.read<EntriesBloc>().add(EntriesEventRefresh());
                },
                label: const Text("refresh"),
                icon: const Icon(Icons.refresh),
              ),
              OutlinedButton.icon(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const EntryCreatePage(),
                    ),
                  );
                },
                label: const Text("add"),
                icon: const Icon(Icons.add),
              ),
            ],
          ),
        ),
        _isMediumScreen(context)
            ? largerScreenFilterWidgets()
            : smallScreenFilterWidgets(),
        BlocBuilder<EntriesBloc, EntriesState>(builder: (context, state) {
          if (state is EntriesStateData) {
            var entries = state.entries;

            for (var i = 0; i < _selectedTags.length; i++) {
              entries = entries
                  .where((e) =>
                      e.tags.map((t) => t.id).contains(_selectedTags[i].id))
                  .toList();
            }
            entries = entries
                .where((e) =>
                    e.name.toLowerCase().contains(
                        _searchTextEditingController.text.toLowerCase()) ||
                    (e.notes != null &&
                        e.notes!.toLowerCase().contains(
                            _searchTextEditingController.text.toLowerCase())))
                .toList();

            return GridView.builder(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 500,
                mainAxisExtent: 90,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemCount: entries.length,
              itemBuilder: (context, index) {
                return EntryListCard(entry: entries[index]);
              },
            );
          } else if (state is EntriesStateError) {
            return Center(
              child: Text(state.error),
            );
          } else if (state is EntriesStateInitial) {
            return const Center(
              child: Text("Please refresh!"),
            );
          } else if (state is EntriesStateLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return const Center(
            child: Text("?"),
          );
        }),
      ]),
    );
  }
}

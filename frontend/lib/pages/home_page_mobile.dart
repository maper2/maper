import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/LoginBloc/LoginBloc.dart';
import 'package:maper_app/bloc/LoginBloc/LoginEvent.dart';
import 'package:maper_app/bloc/TagBloc/bloc.dart';
import 'package:maper_app/main_drawer.dart';
import 'package:maper_app/pages/entries/entry_list_page.dart';
import 'package:maper_app/pages/login_page.dart';
import 'package:maper_app/pages/tags/tag_list_page.dart';

class HomePageMobile extends StatefulWidget {
  const HomePageMobile({super.key});

  @override
  State<HomePageMobile> createState() => _HomePageMobileState();
}

class _HomePageMobileState extends State<HomePageMobile>
    with TickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: _pages.length);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  static final List<Widget> _pages = <Widget>[
    const EntryListPage(),
    const TagListPage(),
  ];
  static const List<Tab> _tabs = <Tab>[
    Tab(text: 'Entries'),
    Tab(text: 'Tags'),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationBlocState>(
      listener: (context, state) {
        if (state is AuthenticationBlocStateLoggedIn) {
          // call any block here that should do something after logging in.

          context.read<EntriesBloc>().add(EntriesEventRefresh());
          context.read<TagBloc>().add(TagEventRefresh());
        }
      },
      child: BlocBuilder<AuthenticationBloc, AuthenticationBlocState>(
        builder: (context, state) {
          if (state is AuthenticationBlocStateLoggedIn) {
            return DefaultTabController(
              initialIndex: 0,
              length: _pages.length,
              child: Scaffold(
                appBar: AppBar(
                  backgroundColor: Theme.of(context).colorScheme.inversePrimary,
                  title: const Text("Maper"),
                  actions: [
                    IconButton(
                      icon: const Icon(Icons.logout),
                      tooltip: 'Logout',
                      onPressed: () {
                        context
                            .read<AuthenticationBloc>()
                            .add(AuthenticationBlocEventLogout());
                        context.read<LoginBloc>().add(LoginLoadInitialData());
                      },
                    ),
                  ],
                  bottom: const TabBar(
                    tabs: _tabs,
                  ),
                ),
                drawer: const MainDrawer(),
                body: TabBarView(
                  children: _pages,
                ),
              ),
            );
          } else {
            return const LoginPage();
          }
        },
      ),
    );
  }
}

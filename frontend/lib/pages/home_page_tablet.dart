import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/AuthenticationBloc/bloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesBloc.dart';
import 'package:maper_app/bloc/EntriesBloc/EntriesEvents.dart';
import 'package:maper_app/bloc/LoginBloc/LoginBloc.dart';
import 'package:maper_app/bloc/LoginBloc/LoginEvent.dart';
import 'package:maper_app/bloc/TagBloc/bloc.dart';
import 'package:maper_app/main_drawer.dart';
import 'package:maper_app/pages/entries/entry_list_page.dart';
import 'package:maper_app/pages/login_page.dart';
import 'package:maper_app/pages/tags/tag_list_page.dart';

class HomePageTablet extends StatefulWidget {
  const HomePageTablet({super.key});

  @override
  State<HomePageTablet> createState() => _HomePageTabletState();
}

class _HomePageTabletState extends State<HomePageTablet> {
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  static final List<Widget> _pages = <Widget>[
    const EntryListPage(),
    const TagListPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationBlocState>(
      listener: (context, state) {
        if (state is AuthenticationBlocStateLoggedIn) {
          // call any block here that should do something after logging in.

          context.read<EntriesBloc>().add(EntriesEventRefresh());
          context.read<TagBloc>().add(TagEventRefresh());
        }
      },
      child: BlocBuilder<AuthenticationBloc, AuthenticationBlocState>(
        builder: (context, state) {
          if (state is AuthenticationBlocStateLoggedIn) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.inversePrimary,
                title: const Text("Maper"),
                actions: [
                  IconButton(
                    icon: const Icon(Icons.logout),
                    tooltip: 'Logout',
                    onPressed: () {
                      context
                          .read<AuthenticationBloc>()
                          .add(AuthenticationBlocEventLogout());
                      context.read<LoginBloc>().add(LoginLoadInitialData());
                    },
                  ),
                ],
              ),
              drawer: const MainDrawer(),
              body: Row(
                children: [
                  NavigationRail(
                    labelType: NavigationRailLabelType.all,
                    onDestinationSelected: (int index) {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                    destinations: const [
                      NavigationRailDestination(
                        icon: Icon(Icons.book_outlined),
                        selectedIcon: Icon(Icons.book),
                        label: Text('Entries'),
                      ),
                      NavigationRailDestination(
                        icon: Icon(Icons.label_outline),
                        selectedIcon: Icon(Icons.label),
                        label: Text('Tags'),
                      ),
                    ],
                    selectedIndex: _selectedIndex,
                  ),
                  const VerticalDivider(thickness: 1, width: 1),
                  Expanded(child: _pages[_selectedIndex])
                ],
              ),
            );
          } else {
            return const LoginPage();
          }
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:maper_app/delete_all_user_data_button.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Profile"),
      ),
      body: ListView(
        padding: const EdgeInsets.all(10),
        children: const [
          //const Text("change password button..."),
          DeleteAllUserDataButton(),
        ],
      ),
    );
  }
}

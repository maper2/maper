import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/TagBloc/TagBloc.dart';
import 'package:maper_app/bloc/TagBloc/TagEvent.dart';
import 'package:maper_app/domain/tag.dart';

class TagCreatePage extends StatefulWidget {
  const TagCreatePage({super.key});

  @override
  State<TagCreatePage> createState() => _TagCreatePageState();
}

class _TagCreatePageState extends State<TagCreatePage> {
  final _formKey = GlobalKey<FormState>();

  final _nameTextEditingController = TextEditingController();

  _handleSubmit() {
    if (_formKey.currentState!.validate()) {
      context.read<TagBloc>().add(
            TagEventCreateTag(
              newTag: NewTag(
                name: _nameTextEditingController.text,
              ),
            ),
          );
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("New Tag"),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _handleSubmit,
        label: const Text("Submit"),
        icon: const Icon(Icons.check),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                autocorrect: false,
                controller: _nameTextEditingController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Name',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FilledButton(
                    onPressed: _handleSubmit,
                    child: const Text("Submit"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

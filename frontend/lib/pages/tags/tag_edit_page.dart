import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/TagBloc/TagBloc.dart';
import 'package:maper_app/bloc/TagBloc/TagEvent.dart';
import 'package:maper_app/domain/tag.dart';

class TagEditPage extends StatefulWidget {
  final Tag tag;
  const TagEditPage({super.key, required this.tag});

  @override
  State<TagEditPage> createState() => _TagEditPageState(editTag: tag);
}

class _TagEditPageState extends State<TagEditPage> {
  Tag editTag;
  _TagEditPageState({required this.editTag});

  final _formKey = GlobalKey<FormState>();

  final _nameTextEditingController = TextEditingController();

  _handleSubmit() {
    if (_formKey.currentState!.validate()) {
      editTag.name = _nameTextEditingController.text;

      context.read<TagBloc>().add(
            TagEventSaveTag(
              tag: editTag,
            ),
          );
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    _nameTextEditingController.text = editTag.name;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Edit Tag"),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _handleSubmit,
        label: const Text("Save"),
        icon: const Icon(Icons.save),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                autocorrect: false,
                controller: _nameTextEditingController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Name',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FilledButton(
                    onPressed: _handleSubmit,
                    child: const Text("Submit"),
                  ),
                  OutlinedButton(
                    onPressed: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          'Do you want to delete Tag ${editTag.name}?',
                        ),
                        action: SnackBarAction(
                          label: 'Delete',
                          onPressed: () {
                            context.read<TagBloc>().add(
                                  TagEventDeleteTag(
                                    tag: editTag,
                                  ),
                                );
                            Navigator.pop(context);
                          },
                        ),
                      ));
                    },
                    child: const Text("Delete"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

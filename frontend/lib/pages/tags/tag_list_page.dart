import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maper_app/bloc/TagBloc/bloc.dart';
import 'package:maper_app/pages/tags/tag_create_page.dart';
import 'package:maper_app/pages/tags/tag_edit_page.dart';

class TagListPage extends StatelessWidget {
  const TagListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const TagCreatePage(),
            ),
          );
          //context.read<TagBloc>().add(TagEventRefresh());
        },
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Wrap(
              spacing: 8.0, // gap between adjacent chips
              runSpacing: 4.0, // gap between lines
              children: [
                OutlinedButton.icon(
                  onPressed: () {
                    context.read<TagBloc>().add(TagEventRefresh());
                  },
                  label: const Text("refresh"),
                  icon: const Icon(Icons.refresh),
                ),
                OutlinedButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const TagCreatePage(),
                      ),
                    );
                  },
                  label: const Text("add"),
                  icon: const Icon(Icons.add),
                ),
              ],
            ),
          ),
          BlocBuilder<TagBloc, TagState>(
            builder: (context, state) {
              if (state is TagStateData) {
                return GridView.builder(
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                  ),
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  itemCount: state.tags.length,
                  itemBuilder: (context, index) {
                    return Card(
                      clipBehavior: Clip.hardEdge,

                      // In many cases, the key isn't mandatory
                      key: ValueKey(state.tags[index]),
                      margin: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 15),
                      child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    TagEditPage(tag: state.tags[index]),
                              ),
                            );
                          },
                          onLongPress: () {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(
                                'Do you want to delete Tag ${state.tags[index].name}?',
                              ),
                              action: SnackBarAction(
                                label: 'Delete',
                                onPressed: () {
                                  context.read<TagBloc>().add(
                                        TagEventDeleteTag(
                                          tag: state.tags[index],
                                        ),
                                      );
                                },
                              ),
                            ));
                          },
                          child: Center(child: Text(state.tags[index].name))),
                    );
                  },
                );
              } else if (state is TagStateError) {
                return Center(
                  child: Text(state.error),
                );
              } else if (state is TagStateInitial) {
                return const Center(
                  child: Text("Please refresh!"),
                );
              } else if (state is TagStateLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }

              return const Center(
                child: Text("?"),
              );
            },
          )
        ],
      ),
    );
  }
}

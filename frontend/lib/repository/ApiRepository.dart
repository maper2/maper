import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:maper_app/domain/entry.dart';
import 'package:maper_app/domain/tag.dart';
import 'package:maper_app/repository/exceptions/SessionExpiredException.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';

class ApiRepository {
  final http.Client httpClient;

  ApiRepository({
    required this.httpClient,
  });

  Future<String> login({
    required String url,
    required String username,
    required String password,
  }) async {
    var uri = Uri.parse("$url/api/auth/login");
    var response = await http.post(uri,
        headers: {"content-type": "application/json"},
        body: convert.jsonEncode({
          "username": username,
          "cleartext_password": password,
        }));
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString('token', jsonResponse["token"]);
      sharedPreferences.setString('username', username);
      sharedPreferences.setString('url', url);

      return jsonResponse["token"];
    } else {
      var jsonResponse = convert.jsonDecode(response.body);
      throw Exception(jsonResponse);
    }
  }

  Future<bool> register({
    required String url,
    required String username,
    required String password,
  }) async {
    var uri = Uri.parse("$url/api/auth/signup");
    var response = await http.post(uri,
        headers: {"content-type": "application/json"},
        body: convert.jsonEncode({
          "username": username,
          "cleartext_password": password,
        }));
    if (response.statusCode == 200) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString('username', username);
      sharedPreferences.setString('url', url);

      return true;
    } else {
      var jsonResponse = convert.jsonDecode(response.body);
      throw Exception(jsonResponse);
    }
  }

  Future<String?> getPresavedUrl() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefUrl = sharedPreferences.getString('url');
    return prefUrl;
  }

  Future<String?> getPresavedUsername() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefUsername = sharedPreferences.getString('username');
    return prefUsername;
  }

  Future<bool> hasToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefToken = sharedPreferences.getString('token');
    bool result = prefToken != null;
    return result;
  }

  Future<String?> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefToken = sharedPreferences.getString('token');
    return prefToken;
  }

  Future<void> logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    // todo POST Logout to the server?

    sharedPreferences.remove("token");
  }

  Future<List<Entry>> getEntries() async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/entries");

      var response = await http.get(
        uri,
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        List<Entry> entries =
            jsonResponse.map((e) => Entry.fromMap(e)).toList().cast<Entry>();
        return entries;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<Entry> createNewEntry(NewEntry newEntry) async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/entries");
      var response = await http.post(
        uri,
        body: convert.jsonEncode(newEntry.toMap()),
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        Entry entry = Entry.fromMap(jsonResponse);
        return entry;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<Entry> saveEntry(Entry entry) async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/entries/${entry.id}");

      var response = await http.put(
        uri,
        body: convert.jsonEncode(entry.toMap()),
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        Entry entry = Entry.fromMap(jsonResponse);
        return entry;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<bool> deleteEntry(Entry entry) async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/entries/${entry.id}");

      var response = await http.delete(
        uri,
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<List<Tag>> getTags() async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/tags");

      var response = await http.get(
        uri,
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        List<Tag> tags =
            jsonResponse.map((e) => Tag.fromMap(e)).toList().cast<Tag>();

        return tags;
      } else if (response.statusCode == 401) {
        throw SessionExpiredException();
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<Tag> createNewTag(NewTag newTag) async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/tags");

      var response = await http.post(
        uri,
        body: convert.jsonEncode(newTag.toMap()),
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );

      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        Tag tag = Tag.fromMap(jsonResponse);

        return tag;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<Tag> saveTag(Tag tag) async {
    String? token = await getToken();

    String? url = await getPresavedUrl();

    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/tags/${tag.id}");
      log(uri.toString());

      var response = await http.put(
        uri,
        body: convert.jsonEncode(tag.toMap()),
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );

      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        Tag tag = Tag.fromMap(jsonResponse);

        return tag;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }

  Future<bool> deleteTag(Tag tag) async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/tags/${tag.id}");

      var response = await http.delete(
        uri,
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }
  
  Future<bool> delteAllUserData() async {
    String? token = await getToken();

    String? url = await getPresavedUrl();
    if (token != null && url != null) {
      var uri = Uri.parse("$url/api/users/me/delete_all_data");

      var response = await http.delete(
        uri,
        headers: {
          "content-type": "application/json",
          "authorization": "Bearer $token",
        },
      );
      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception(response.body);
      }
    } else {
      throw Exception("url or token undefined");
    }
  }
}

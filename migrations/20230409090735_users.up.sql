-- Add up migration script here
CREATE TABLE IF NOT EXISTS users (
    id uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    username TEXT NOT NULL unique,
    password_hash TEXT NOT NULL
);

-- Add up migration script here
CREATE TABLE IF NOT EXISTS tags (
    id uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    name TEXT NOT NULL,
    created_on TIMESTAMP NOT NULL DEFAULT NOW(),
    user_id uuid NOT NULL,
    UNIQUE (name, user_id)
);

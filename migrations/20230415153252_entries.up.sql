-- Add up migration script here
CREATE TABLE IF NOT EXISTS entries (
    id uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    name TEXT NOT NULL,
    url TEXT NOT NULL,
    notes TEXT,
    user_id uuid NOT NULL,
    created_on TIMESTAMP NOT NULL DEFAULT NOW(),
    UNIQUE (name, user_id)

);

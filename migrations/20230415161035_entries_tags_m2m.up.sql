-- Add up migration script here
CREATE TABLE IF NOT EXISTS entries_tags (
    id uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    tag_id uuid NOT NULL,
    entry_id uuid NOT NULL
);

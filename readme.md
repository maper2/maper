# Maper - Your friendly neighborhood "save my links" application


## Develop

Run and watch for changes
```
MAPER_DB_STRING=postgres://maper:supersecretpassword@localhost:5432/maper RUST_LOG=debug cargo watch -q -c -w src -w templates -x run
```

### New Migration

```
sqlx migrate add -r <name>
```


## Environment Variables

RUST_LOG

- debug
- info
- warn
- error


MAPER_DB_STRING

```
postgres://<user>:<password>@<host>:<port>/<dbmane>
```


## API using JWT

### Signup
```
curl -X POST http://localhost:3000/api/auth/signup -H "content-type: application/json" -d '{"username": "xxxx", "cleartext_password": "xxxx"}' 

```

### Login
The token is also saved in a cookie.

```
x 
```


### Logout
Overwrites the cookie. In order to destroy the token on the client, simply delete it. It would be nice if we could invalidate the token...

```
curl -X GET http://localhost:3000/api/auth/logout -H "content-type: application/json" -H "authorization: Bearer xxxxx"
```
xxxxx is the token returned upon login.

### Get Entries

```
curl -X GET http://localhost:3000/api/entries -H "content-type: application/json" -H "authorization: Bearer xxxxx"
```

### Create new Entry
```
curl -X POST http://localhost:3000/api/entries -H "content-type: application/json" -H "authorization: Bearer xxxxx" -d 'xxxx'
```
where xxxx is the entry data in json.

### Update entry

```
curl -X PUT http://localhost:3000/api/entries/XXXX -H "content-type: application/json" -H "authorization: Bearer xxxxx" -d 'xxxx'
```
Parameters: Entry ID, Token and Data

### Delete entry

```
curl -X DELETE http://localhost:3000/api/entries/XXXX -H "content-type: application/json" -H "authorization: Bearer xxxxx" 
```
Parameters: Entry ID


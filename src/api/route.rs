use crate::{
    jwt_authentication::{
        jwt_auth::auth_layer,
        views::{
            get_me_handler, health_checker_handler, login_user_handler_api, logout_handler,
            protected, register_user_handler,
        },
    },
    types::app_state::AppState,
};
use axum::{
    middleware,
    routing::{delete, put},
    {
        routing::{get, post},
        Router,
    },
};

use super::views::{
    delete_user_data::delete_user_data,
    entry::{create_new_entry_api, delete_entry_api, display_entries_api, update_entry_api},
    signup::signup_api,
};

use crate::api::views::tag::{create_new_tag_api, delete_tag_api, list_tags_api, update_tag_api};

pub fn create_router(app_state: AppState) -> Router<AppState> {
    let router_authenticated = Router::new()
        .route("/protected", get(protected))
        .route("/api/auth/logout", get(logout_handler))
        .route("/api/users/me", get(get_me_handler))
        .route("/api/users/me/delete_all_data", delete(delete_user_data))
        .route("/api/entries", get(display_entries_api))
        .route("/api/entries", post(create_new_entry_api))
        .route("/api/entries/:entryId", put(update_entry_api))
        .route("/api/entries/:entryId", delete(delete_entry_api))
        .route("/api/tags", get(list_tags_api))
        .route("/api/tags", post(create_new_tag_api))
        .route("/api/tags/:tagId", put(update_tag_api))
        .route("/api/tags/:tagId", delete(delete_tag_api));

    let router_open = Router::new()
        .route("/api/healthchecker", get(health_checker_handler))
        .route("/api/auth/register", post(register_user_handler))
        .route("/api/auth/login", post(login_user_handler_api))
        .route("/api/auth/signup", post(signup_api));

    Router::new()
        .merge(router_authenticated)
        .route_layer(middleware::from_fn_with_state(
            app_state.clone(),
            auth_layer,
        ))
        .merge(router_open)
}

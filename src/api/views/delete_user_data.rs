use crate::{
    api::types::response::ErrorResponse,
    types::{app_state::AppState, user::User},
};
use axum::{extract::State, http::StatusCode, response::IntoResponse, Extension, Json};

pub async fn delete_user_data(
    State(state): State<AppState>,
    Extension(user): Extension<User>,
) -> impl IntoResponse {
    match state.store.delte_all_user_data(user.id).await {
        Ok(_) => Ok(StatusCode::OK),
        Err(_e) => Err((
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(ErrorResponse {
                errors: vec![format!("Could not delete your user data",)],
            }),
        )),
    }
}

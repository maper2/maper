use std::vec;

use axum::{
    extract::{Path, Query, State},
    http::{HeaderMap, StatusCode},
    response::IntoResponse,
    Extension, Json,
};

use crate::{
    api::types::response::ErrorResponse,
    types::{
        app_state::AppState,
        entry::{Entry, EntryId, NewEntry},
        user::User,
    },
    views::entry::EntriesFilterParams,
};

pub async fn display_entries_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    params: Query<EntriesFilterParams>,
) -> impl IntoResponse {
    let Ok(entries) = state
        .store
        .get_entries(
            params.limit,
            params.offset.unwrap_or_default(),
            params.search.clone(),
            params.tag_id,
            user,
        )
        .await
    else {
        panic!("some error for entries");
    };
    let mut headers = HeaderMap::new();
    headers.insert(
        "Content-Type",
        "application/json; charset=utf-8".parse().unwrap(),
    );

    (headers, Json(entries))
}

pub async fn create_new_entry_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    Json(new_entry): Json<NewEntry>,
) -> impl IntoResponse {
    tracing::event!(tracing::Level::WARN, "NEW ENTRY {:?}", &new_entry);

    match state.store.save_new_entry(new_entry, user).await {
        Ok(entry) => {
            tracing::event!(tracing::Level::INFO, "Saved new entry {:?}", entry);
            Ok((StatusCode::OK, Json(entry)))
        }
        Err(e) => {
            tracing::event!(tracing::Level::ERROR, "Error saving entry {:#?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![format!("Error saving entry {:#?}", e)],
                }),
            ))
        }
    }
}

pub async fn update_entry_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    Json(entry): Json<Entry>,
) -> impl IntoResponse {
    match state.store.update_entry(entry, user).await {
        Ok(entry) => {
            tracing::event!(
                tracing::Level::INFO,
                "Updated entry as seen from view {:?}",
                entry
            );
            Ok((StatusCode::OK, Json(entry)))
        }
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when updating entry {:?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![format!("Error when updating entry {:#?}", e)],
                }),
            ))
        }
    }
}

pub async fn delete_entry_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    Path(entry_id): Path<EntryId>,
) -> impl IntoResponse {
    match state.store.delete_entry(entry_id, user).await {
        Ok(_) => Ok((StatusCode::OK, Json(()))),
        Err(e) => {
            tracing::event!(tracing::Level::ERROR, "Error when deleting entry {:?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![format!("Error when deleting entry {:#?}", e)],
                }),
            ))
        }
    }
}

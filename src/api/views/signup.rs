use axum::{extract::State, http::StatusCode, response::IntoResponse, Json};

use crate::{
    api::types::response::ErrorResponse,
    types::{app_state::AppState, user::NewUser},
    views::authentication::handle_user_signup,
};

pub async fn signup_api(
    State(state): State<AppState>,
    Json(new_user): Json<NewUser>,
) -> impl IntoResponse {
    match handle_user_signup(&state, new_user).await {
        Ok(user) => Ok((StatusCode::OK, Json(user))),
        Err(_e) => Err((
            StatusCode::BAD_REQUEST,
            Json(ErrorResponse {
                errors: vec![format!(
                    "Could not create the user, maybe try another username?",
                )],
            }),
        )),
    }
}

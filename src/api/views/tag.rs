use axum::{
    extract::{Path, State},
    http::{HeaderMap, StatusCode},
    response::IntoResponse,
    Extension, Json,
};

use crate::{
    api::types::response::ErrorResponse,
    types::{
        app_state::AppState,
        tag::{NewTag, Tag, TagId},
        user::User,
    },
};

pub async fn list_tags_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
) -> impl IntoResponse {
    let Ok(tags) = state.store.get_tags(None, 0, user).await else {
        panic!("some error");
    };

    let mut headers = HeaderMap::new();
    headers.insert(
        "Content-Type",
        "application/json; charset=utf-8".parse().unwrap(),
    );

    (headers, Json(tags))
}

pub async fn create_new_tag_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    Json(new_tag): Json<NewTag>,
) -> impl IntoResponse {
    match state.store.save_new_tag(new_tag, user).await {
        Ok(tag) => {
            tracing::event!(tracing::Level::WARN, "Saved new tag {:?}", tag);
            Ok((StatusCode::OK, Json(tag)))
        }
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when saving new tag {:?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![format!("Error when saving new tag {:#?}", e)],
                }),
            ))
        }
    }
}

pub async fn update_tag_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    Json(tag): Json<Tag>,
) -> impl IntoResponse {
    match state.store.update_tag(tag, user).await {
        Ok(tag) => {
            tracing::event!(tracing::Level::WARN, "Updated tag {:?}", tag);
            Ok((StatusCode::OK, Json(tag)))
        }
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when updating tag {:?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![format!("Error when updating tag {:#?}", e)],
                }),
            ))
        }
    }
}

pub async fn delete_tag_api(
    Extension(user): Extension<User>,
    State(state): State<AppState>,
    Path(tag_id): Path<TagId>,
) -> impl IntoResponse {
    match state.store.delete_tag(tag_id, user).await {
        Ok(_) => Ok((StatusCode::OK, Json(()))),
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when deleting tag {:?}", e);
            Err((
                StatusCode::BAD_REQUEST,
                Json(ErrorResponse {
                    errors: vec![format!("Error when deleting tag {:#?}", e)],
                }),
            ))
        }
    }
}

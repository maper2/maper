use argon2::{
    password_hash::{
        rand_core::OsRng, Error, PasswordHash, PasswordHasher, PasswordVerifier, SaltString,
    },
    Argon2,
};

pub fn hash_password(cleartext_password: String) -> Result<String, Error> {
    let salt = SaltString::generate(&mut OsRng);

    // Argon2 with default params (Argon2id v19)
    let argon2 = Argon2::default();

    // Hash password to PHC string ($argon2id$v=19$...)
    let hash = argon2
        .hash_password(cleartext_password.as_bytes(), &salt)?
        .to_string();

    Ok(hash)
}

pub fn verify_password_hash(cleartext_password: String, password_hash: String) -> bool {
    let Ok(parsed_hash) = PasswordHash::new(&password_hash) else {
        tracing::event!(tracing::Level::ERROR, "Could not compute passwort hash in verify_password_hash!");
        return false;
    };
    Argon2::default()
        .verify_password(cleartext_password.as_bytes(), &parsed_hash)
        .is_ok()
}

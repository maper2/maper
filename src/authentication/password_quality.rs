use passwords::analyzer;


pub fn check_password_quality(password: String) -> Vec<String>{
    let mut errors: Vec<String> = vec![];

    let analyzed = analyzer::analyze(password);


    if analyzed.length() < 8 {
        errors.push("The password must have at least 8 characters".to_owned());
    }

    if analyzed.numbers_count() == 0 {
        errors.push("The password must have at least one number".to_owned());
    }

    if analyzed.lowercase_letters_count() == 0{
        errors.push("The password must have at least one lowercase letter".to_owned());
    }

    if analyzed.uppercase_letters_count() == 0{
        errors.push("The password must have at least one uppercase letter".to_owned());
    }


    errors

}


#[cfg(test)]
mod tests {
    use crate::authentication::password_quality::check_password_quality;

    #[test]
    fn test_acceptable_password() {
        let errors = check_password_quality("gk5Age6k".to_owned());
        assert_eq!(errors.len(), 0);
    }

    #[test]
    fn test_password_without_uppercase_letters() {
        let errors = check_password_quality("aaaaaa236235253aaaaaaa".to_owned());
        assert_eq!(errors.len(), 1);
    }

    #[test]
    fn test_password_without_lowercase_letters() {
        let errors = check_password_quality("AAAAAAAAAA2340293842ASLDKALSKDJ".to_owned());
        assert_eq!(errors.len(), 1);
    }

    #[test]
    fn test_password_without_numbers() {
        let errors = check_password_quality("SFAEOGJjsldkajegLAKSDJAEG".to_owned());
        assert_eq!(errors.len(), 1);
    }


    #[test]
    fn test_short_password() {
        let errors = check_password_quality("fSgk3".to_owned());
        assert_eq!(errors.len(), 1);
    }



}
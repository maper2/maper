use rand::{thread_rng, Rng};
use chrono::Duration as ChronoDuration;
use time::Duration as TimeDuration;

#[derive(Debug, Clone)]
pub struct Config {
    /// connection url for the POSTGRES database
    pub database_url: String,
    pub jwt_secret: String,
    pub jwt_expires_in: ChronoDuration,
    pub jwt_cookie_maxage: TimeDuration,
    /// Port the webserver should bind to
    pub app_port: u16,
    /// Give the users a possibility to specify their secret for session
    /// authentication instead of re-rolling it every time at server startup
    /// will be ignored and rolled when the provided source string is shorter than 64b
    pub secret: [u8; 64],
}

impl Config {
    pub fn init() -> Config {
        let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let jwt_secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");
        let jwt_expires_in =  ChronoDuration::hours(std::env::var("JWT_EXPIRES_IN_HOURS").expect("JWT_EXPIRES_IN_HOURS must be set").parse::<i64>().expect("that this string for JWT_EXPIRES_IN_HOURS is parseable as i64"));
        let jwt_cookie_maxage = TimeDuration::hours(std::env::var("JWT_COOKIE_MAXAGE_HOURS").expect("JWT_COOKIE_MAXAGE_HOURS must be set").parse::<i64>().expect("that this string for JWT_COOKIE_MAXAGE_HOURS is parseable as i64"));
        let app_port = std::env::var("MAPER_PORT").unwrap_or("3000".to_owned());
        let secret_string = std::env::var("MAPER_SECRET").unwrap_or("".to_owned());
        
        
        let secret_bytes = secret_string.into_bytes();
        let secret_bytes_len = secret_bytes.len();
        let secret: [u8; 64]  = if secret_bytes_len < 64 {
            tracing::event!(tracing::Level::WARN, "Rolling a secret since the provided one was too short with {:?} bytes", secret_bytes_len);

            let mut rng = thread_rng();
            let mut secret: [u8; 64] = [0u8; 64];
            rng.fill(&mut secret);
            secret
        } else {         

            secret_bytes.chunks(64).next().expect("the byte slice to have at lease 64 elements").try_into().unwrap()
        };


        Config {
            database_url,
            jwt_secret,
            jwt_expires_in,
            jwt_cookie_maxage,
            app_port: app_port.parse::<u16>().unwrap_or(3000),
            secret,
        }
    }
}
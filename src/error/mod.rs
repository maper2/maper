pub enum Error {
    /// any error that originates in the database
    DatabaseError(sqlx::Error),
}

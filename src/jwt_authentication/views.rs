use argon2::{password_hash::SaltString, Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use axum::{
    extract::State,
    http::{header, Response, StatusCode},
    response::IntoResponse,
    Extension, Json,
};
use axum_login::tower_sessions::cookie::{Cookie, SameSite};
use jsonwebtoken::{encode, EncodingKey, Header};
use rand::rngs::OsRng;
use serde_json::json;
use sqlx::{postgres::PgRow, Row};

use crate::types::{
    app_state::AppState,
    user::{filter_user_record, NewUser, User},
};

use super::jwt_auth_2::{AuthError, Claims};

pub async fn protected(claims: Claims) -> Result<String, AuthError> {
    // Send the protected data to the user
    Ok(format!(
        "Welcome to the protected area :)\nYour data:\n{claims}",
    ))
}

pub async fn register_user_handler(
    State(state): State<AppState>,
    Json(body): Json<NewUser>,
) -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    let user_exists: Option<bool> =
        sqlx::query_scalar("SELECT EXISTS(SELECT 1 FROM users WHERE username = $1)")
            .bind(body.username.to_owned().to_ascii_lowercase())
            .fetch_one(&state.store.connection)
            .await
            .map_err(|e| {
                let error_response = serde_json::json!({
                    "status": "fail",
                    "error": format!("Database error: {}", e),
                });
                (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
            })?;

    if let Some(exists) = user_exists {
        if exists {
            let error_response = serde_json::json!({
                "status": "fail",
                "error": "User with that email already exists",
            });
            return Err((StatusCode::CONFLICT, Json(error_response)));
        }
    }

    let salt = SaltString::generate(&mut OsRng);
    let hashed_password = Argon2::default()
        .hash_password(body.cleartext_password.as_bytes(), &salt)
        .map_err(|e| {
            let error_response = serde_json::json!({
                "status": "fail",
                "error": format!("Error while hashing password: {}", e),
            });
            (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
        })
        .map(|hash| hash.to_string())?;

    let user = sqlx::query("INSERT INTO users (name,password) VALUES ($1, $2) RETURNING *")
        .bind(body.username.to_string())
        .bind(hashed_password)
        .map(|row: PgRow| User {
            id: row.get("id"),
            username: row.get("username"),
            password_hash: row.get("password_hash"),
        })
        .fetch_one(&state.store.connection)
        .await
        .map_err(|e| {
            let error_response = serde_json::json!({
                "status": "fail",
                "error": format!("Database error: {}", e),
            });
            (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
        })?;

    let user_response = serde_json::json!({"status": "success","data": serde_json::json!({
        "user": filter_user_record(&user)
    })});

    Ok(Json(user_response))
}

pub async fn login_user_handler_api(
    State(state): State<AppState>,
    Json(new_user): Json<NewUser>,
) -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    tracing::event!(tracing::Level::WARN, "login_user_handler_api");
    println!("login user handler api");

    let user = sqlx::query("SELECT * FROM users WHERE username = $1")
        .bind(new_user.username.to_string())
        .map(|row: PgRow| User {
            id: row.get("id"),
            username: row.get("username"),
            password_hash: row.get("password_hash"),
        })
        .fetch_optional(&state.store.connection)
        .await
        .map_err(|e| {
            let error_response = serde_json::json!({
                "status": "error",
                "error": format!("Database error: {}", e),
            });
            (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
        })?
        .ok_or_else(|| {
            let error_response = serde_json::json!({
                "status": "fail",
                "error": "Invalid email or password",
            });
            (StatusCode::FORBIDDEN, Json(error_response))
        })?;
    tracing::event!(
        tracing::Level::INFO,
        "login_user_handler_api user_id: {} username: {}",
        &user.id,
        &user.username,
    );

    let is_valid = match PasswordHash::new(&user.password_hash) {
        Ok(parsed_hash) => Argon2::default()
            .verify_password(new_user.cleartext_password.as_bytes(), &parsed_hash)
            .map_or(false, |_| true),
        Err(_) => false,
    };

    if !is_valid {
        let error_response = serde_json::json!({
            "status": "fail",
            "error": "Invalid email or password"
        });
        return Err((StatusCode::FORBIDDEN, Json(error_response)));
    }

    let now = chrono::Utc::now();
    //let iat = now.timestamp() as usize;
    let exp = (now + state.config.jwt_expires_in).timestamp() as usize;
    let claims = Claims {
        exp,
        user_id: user.id,
    };

    /*

    let claims: TokenClaims = TokenClaims {
        sub: user.id.to_string(),
        exp,
        iat,
    };
     */

    let token = encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(state.config.jwt_secret.as_ref()),
    )
    .unwrap();

    /*
    let cookie = Cookie::build(format!("token {}", &token))
        .path("/")
        .max_age(state.config.jwt_cookie_maxage)
        .same_site(SameSite::Lax)
        .http_only(true)
        .finish();
    */
    let response = Response::new(json!({"status": "success", "token": token}).to_string());
    /*
    let mut response = Response::new(json!({"status": "success", "token": token}).to_string());
    response
        .headers_mut()
        .insert(header::SET_COOKIE, cookie.to_string().parse().unwrap());
    */
    Ok(response)
}

pub async fn logout_handler() -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    let cookie = Cookie::build("token")
        .path("/")
        .max_age(time::Duration::hours(-1))
        .same_site(SameSite::Lax)
        .http_only(true);

    let mut response = Response::new(json!({"status": "success"}).to_string());
    response
        .headers_mut()
        .insert(header::SET_COOKIE, cookie.to_string().parse().unwrap());
    Ok(response)
}

pub async fn get_me_handler(
    Extension(user): Extension<User>,
) -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    // this can be rewritten with a JsonResponse<User> (or similar)
    let json_response = serde_json::json!({
        "status":  "success",
        "data": serde_json::json!({
            "user": filter_user_record(&user)
        })
    });

    Ok(Json(json_response))
}

pub async fn health_checker_handler() -> impl IntoResponse {
    const MESSAGE: &str = "Maper is running";

    let json_response = serde_json::json!({
        "status": "success",
        "message": MESSAGE
    });

    Json(json_response)
}

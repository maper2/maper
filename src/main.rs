use crate::{api::route::create_router, store::Store};
use axum::{
    error_handling::HandleErrorLayer,
    http::{
        header::{ACCEPT, AUTHORIZATION, CONTENT_TYPE},
        Method, StatusCode,
    },
    routing::{delete, get, post},
    BoxError, Router,
};
use axum_login::{
    login_required,
    tower_sessions::{Expiry, MemoryStore, SessionManagerLayer},
    AuthManagerLayerBuilder,
};
use std::net::SocketAddr;
use time::Duration;
use tower::ServiceBuilder;
use tower_http::services::ServeDir;
use tower_http::{
    cors::{Any, CorsLayer},
    trace::TraceLayer,
};
mod store;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
mod api;
mod jwt_authentication;
mod types;

use types::app_state::AppState;
mod authentication;
mod views;
use views::{
    authentication::{login_form, login_handler, logout_handler, signup_form, signup_post},
    entry::{create_new_entry, delete_entry, display_entries, update_entry, update_entry_form},
    minor::index,
    tag::{create_new_tag, delete_tag, tag_select, update_tag, update_tag_form},
};
mod templates;

mod config;
use config::Config;

#[tokio::main]
async fn main() {
    // init tracing setup
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "example_templates=debug".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    // parse environment variables and set up db connection
    let config: Config = Config::init();

    let store = store::Store::new(config.database_url.clone().as_str()).await;

    // run db migrations
    sqlx::migrate!()
        .run(&store.clone().connection)
        .await
        .expect("Cannot run migration");

    let state = AppState {
        store: store.clone(),
        config: config.clone(),
    };

    // authentication setup

    // the usage of memory store leads to a loss of all sessions when the server is restarted
    // so when developing, one must login after every new build and server restart
    // apparently, one should use PostgresStore here, but SessionStore is not implemented for postgres store.

    let session_store = MemoryStore::default();
    let session_layer = SessionManagerLayer::new(session_store)
        .with_secure(false)
        .with_expiry(Expiry::OnInactivity(Duration::days(14)));

    let auth_service = ServiceBuilder::new()
        .layer(HandleErrorLayer::new(|_: BoxError| async {
            StatusCode::BAD_REQUEST
        }))
        .layer(AuthManagerLayerBuilder::new(store.clone(), session_layer).build());

    // api router with cors

    let cors = CorsLayer::new()
        .allow_origin(Any)
        .allow_methods([Method::GET, Method::POST, Method::PUT, Method::DELETE])
        .allow_headers([AUTHORIZATION, ACCEPT, CONTENT_TYPE]);

    let api_router = create_router(state.clone()).layer(cors);

    // templates router for webapp

    let protected_router = Router::new()
        .route("/tags", get(tag_select))
        .route("/tags", post(create_new_tag))
        .route("/tags/:tag_id", get(update_tag_form))
        .route("/tags/:tag_id", post(update_tag))
        .route("/tags/:tag_id", delete(delete_tag))
        .route("/entries", get(display_entries))
        .route("/entries", post(create_new_entry))
        .route("/entries/:entry_id", get(update_entry_form))
        .route("/entries/:entry_id", post(update_entry))
        .route("/entries/:entry_id", delete(delete_entry))
        .route_layer(login_required!(Store, login_url = "/login"));

    let open_router = Router::new()
        .route("/", get(index))
        .nest_service("/static", ServeDir::new("static"))
        .route("/signup", get(signup_form))
        .route("/signup", post(signup_post))
        .route("/login", get(login_form))
        .route("/login", post(login_handler))
        .route("/logout", get(logout_handler));

    let combined_router = Router::new()
        .merge(protected_router)
        .route_layer(login_required!(Store, login_url = "/login"))
        .merge(open_router)
        .merge(api_router)
        .layer(auth_service)
        .layer(TraceLayer::new_for_http());
    /*
    .layer(
        // Automatic Redirects for 404 Responses to the Login page.
        ServiceBuilder::new()
            .layer(session_layer)
            .layer(auth_service)
            .map_response(|r: Response| {
                if r.status() == StatusCode::UNAUTHORIZED {
                    Redirect::to("/login").into_response()
                } else {
                    r
                }
            }),
    );
    */

    // build our application with some routes
    let app = combined_router.with_state(state);
    // let app = Router::new().merge(combined_router);
    // let app = Router::new().merge(combined_router).merge(api_router);

    // run it
    let port: u16 = config.clone().app_port;
    let addr = SocketAddr::from(([0, 0, 0, 0], port));

    let listener = tokio::net::TcpListener::bind(addr)
        .await
        .expect("that we could bind to the given address");
    tracing::debug!(
        "🚀 listening on {}",
        listener
            .local_addr()
            .expect("that we could obtain the local_addr from the listener")
    );

    axum::serve(listener, app)
        .await
        .expect("that the axum server could start");
}

use crate::types::entry::{Entry, EntryId, NewEntry, UpdatedEntry};
use crate::types::tag::{NewTag, Tag, TagId};
use crate::types::user::{LoginFormData, NewUser, User};
use axum::async_trait;
use axum_login::{AuthnBackend, UserId};
use sqlx::postgres::{PgPool, PgPoolOptions, PgRow};
use sqlx::Row;
use uuid::Uuid;

use crate::authentication::password_hash::{hash_password, verify_password_hash};

#[derive(Debug, Clone)]
pub struct Store {
    pub connection: PgPool,
}

fn wrap_string_where_icontains(s: Option<String>) -> String {
    match s {
        Some(s) => format!("%{}%", s),
        None => "%%".to_owned(),
    }
}

#[async_trait]
impl AuthnBackend for Store {
    type User = User;
    type Credentials = LoginFormData;
    type Error = sqlx::Error;

    async fn authenticate(
        &self,
        creds: Self::Credentials,
    ) -> Result<Option<Self::User>, Self::Error> {
        let user: Option<Self::User> = sqlx::query_as("select * from users where username = ? ")
            .bind(creds.username)
            .fetch_optional(&self.connection)
            .await?;

        Ok(user.filter(|user| {
            verify_password_hash(creds.password, user.password_hash.clone())
            // We're using password-based authentication--this
            // works by comparing our form input with an argon2
            // password hash.
        }))
    }

    async fn get_user(&self, user_id: &UserId<Self>) -> Result<Option<Self::User>, Self::Error> {
        tracing::event!(tracing::Level::DEBUG, "get_user id {}", &user_id);

        let user: Option<User> = sqlx::query_as("select * from users where id = $1")
            .bind(user_id)
            .fetch_optional(&self.connection)
            .await?;

        match user {
            Some(user) => {
                tracing::event!(tracing::Level::DEBUG, "get_user user {}", &user.username);
                Ok(Some(user))
            }
            None => {
                tracing::event!(tracing::Level::ERROR, "get_user NONE");
                Ok(None)
            }
        }
        // Ok(user)
    }
}

pub type AuthSession = axum_login::AuthSession<Store>;

impl Store {
    pub async fn new(db_url: &str) -> Self {
        let db_pool = match PgPoolOptions::new()
            .max_connections(5)
            .connect(db_url)
            .await
        {
            Ok(pool) => pool,
            Err(e) => panic!("Couldn't establish DB connection: {}", e),
        };
        Store {
            connection: db_pool,
        }
    }

    pub async fn get_tags(
        &self,
        limit: Option<i32>,
        offset: i32,
        user: User,
    ) -> Result<Vec<Tag>, sqlx::Error> {
        match sqlx::query("SELECT * from tags  where user_id = $3 order by name LIMIT $1 OFFSET $2")
            .bind(limit)
            .bind(offset)
            .bind(user.id)
            .map(|row: PgRow| Tag {
                id: TagId(row.get("id")),
                name: row.get("name"),
            })
            .fetch_all(&self.connection)
            .await
        {
            Ok(tags) => Ok(tags),
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e)
            }
        }
    }

    async fn query_tags_for_entry(&self, mut entry: Entry) -> Entry {
        let Ok(tags) = sqlx::query(
            "
select t.* from tags t inner join entries_tags et on et.tag_id = t.id where et.entry_id = $1::uuid;
           ",
        )
        .bind(entry.id.0)
        .map(|row: PgRow| Tag {
            id: TagId(row.get("id")),
            name: row.get("name"),
        })
        .fetch_all(&self.connection)
        .await
        else {
            panic!("Some error when querying tags for entry")
        };

        entry.tags = tags;
        entry
    }

    async fn query_tags_for_entries(&self, entries: Vec<Entry>) -> Vec<Entry> {
        let mut result: Vec<Entry> = vec![];

        for mut entry in entries {
            entry = self.query_tags_for_entry(entry).await;
            result.push(entry)
        }

        result
    }

    pub async fn get_entries(
        &self,
        limit: Option<i32>,
        offset: i32,
        search: Option<String>,
        tag_id: Option<Uuid>,
        user: User,
    ) -> Result<Vec<Entry>, sqlx::Error> {
        match tag_id {
            Some(tag_id) => {
                // this match statement selects all entries
                match sqlx::query(
                    "select * from entries e
                    inner join entries_tags et
                    on et.entry_id = e.id
                    where et.tag_id = $4 and e.user_id = $5 and ( LOWER(e.name) like LOWER($3) OR LOWER(e.url) like LOWER($3) or LOWER(e.notes) like LOWER($3) )
                    order by e.created_on desc
                    LIMIT $1 OFFSET $2",
                )
                .bind(limit)
                .bind(offset)
                .bind(wrap_string_where_icontains(search))
                .bind(tag_id)
                .bind(user.id)
                .map(|row: PgRow| Entry {
                    // we are selecting from entry_tags, so id is not the entry_id
                    id: EntryId(row.get("entry_id")),
                    name: row.get("name"),
                    url: row.get("url"),
                    notes: row.get("notes"),
                    tags: vec![],
                })
                .fetch_all(&self.connection)
                .await
                {
                    Ok(entries) => {
                        // if successful, we query the tags for each entry and update
                        // the entry with its tags.

                        let result_entries = self.query_tags_for_entries(entries).await;

                        Ok(result_entries)
                    }
                    Err(e) => {
                        tracing::event!(tracing::Level::ERROR, "{:?}", e);
                        Err(e) //Err(Error::DatabaseQueryError) ??
                    }
                }
            }
            None => {
                // this match statement selects all entries
                match sqlx::query(
                    "SELECT * from entries e 
                            WHERE user_id = $4 
                            and ( LOWER(e.name) like LOWER($3) OR LOWER(e.url) like LOWER($3) 
                            or LOWER(e.notes) like LOWER($3)) 
                            order by e.created_on desc 
                            LIMIT $1 OFFSET $2",
                )
                .bind(limit)
                .bind(offset)
                .bind(wrap_string_where_icontains(search))
                .bind(user.id)
                .map(|row: PgRow| Entry {
                    id: EntryId(row.get("id")),
                    name: row.get("name"),
                    url: row.get("url"),
                    notes: row.get("notes"),
                    tags: vec![],
                })
                .fetch_all(&self.connection)
                .await
                {
                    Ok(entries) => {
                        // if successful, we query the tags for each entry and update
                        // the entry with its tags.
                        Ok(self.query_tags_for_entries(entries).await)
                    }
                    Err(e) => {
                        tracing::event!(tracing::Level::ERROR, "{:?}", e);
                        Err(e) //Err(Error::DatabaseQueryError) ??
                    }
                }
            }
        }
    }

    pub async fn save_new_tag(&self, new_tag: NewTag, user: User) -> Result<Tag, sqlx::Error> {
        match sqlx::query(
            "INSERT INTO tags (name, user_id)
            VALUES ($1, $2) returning *;
     ",
        )
        .bind(new_tag.name)
        .bind(user.id)
        .map(|row: PgRow| Tag {
            id: TagId(row.get("id")),
            name: row.get("name"),
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(tag) => Ok(tag),
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn get_tag(&self, tag_id: TagId, user: User) -> Result<Tag, sqlx::Error> {
        match sqlx::query(
            "select * from tags  where id = $1 and user_id = $2;
     ",
        )
        .bind(tag_id.0)
        .bind(user.id)
        .map(|row: PgRow| Tag {
            id: TagId(row.get("id")),
            name: row.get("name"),
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(tag) => Ok(tag),
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn update_tag(&self, tag: Tag, user: User) -> Result<Tag, sqlx::Error> {
        match sqlx::query(
            "UPDATE tags set name = $2 where id = $1 and user_id = $3 RETURNING *;
     ",
        )
        .bind(tag.id.0)
        .bind(tag.name)
        .bind(user.id)
        .map(|row: PgRow| Tag {
            id: TagId(row.get("id")),
            name: row.get("name"),
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(tag) => Ok(tag),
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn delete_tag(&self, tag_id: TagId, user: User) -> Result<(), sqlx::Error> {
        // todo: guard the first delete statement on entries_tags by user-id

        match sqlx::query("DELETE from entries_tags where tag_id = $1")
            .bind(tag_id.0)
            .execute(&self.connection)
            .await
        {
            Ok(_) => {
                match sqlx::query("DELETE from tags where id = $1 and user_id = $2;")
                    .bind(tag_id.0)
                    .bind(user.id)
                    .execute(&self.connection)
                    .await
                {
                    Ok(_) => Ok(()),
                    Err(e) => {
                        tracing::event!(
                            tracing::Level::ERROR,
                            "Error when deleting from tags: {:?}",
                            e
                        );
                        Err(e)
                    }
                }
            }
            Err(e) => {
                tracing::event!(
                    tracing::Level::ERROR,
                    "Error when deleting from entries_tags: {:?}",
                    e
                );
                Err(e)
            }
        }
    }

    pub async fn save_new_entry(
        &self,
        new_entry: NewEntry,
        user: User,
    ) -> Result<Entry, sqlx::Error> {
        match sqlx::query(
            "INSERT INTO entries (name, url, notes, user_id)
            VALUES ($1, $2, $3, $4) returning *;
     ",
        )
        .bind(new_entry.name)
        .bind(new_entry.url)
        .bind(new_entry.notes)
        .bind(user.id)
        .map(|row: PgRow| Entry {
            id: EntryId(row.get("id")),
            name: row.get("name"),
            url: row.get("url"),
            notes: row.get("notes"),
            tags: vec![],
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(entry) => {
                // if successful, save tags to entry, then return it

                for tag_id in new_entry.tag_ids {
                    if let Err(e) = self.save_entry_tag(entry.id.clone(), tag_id).await {
                        tracing::event!(tracing::Level::ERROR, "{:?}", e);
                        return Err(e);
                    }
                }

                Ok(entry)
            }
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn save_entry_tag(
        &self,
        entry_id: EntryId,
        tag_id: TagId,
    ) -> Result<(), sqlx::Error> {
        match sqlx::query(
            "INSERT INTO entries_tags (entry_id, tag_id)
            VALUES ($1, $2);
     ",
        )
        .bind(entry_id.0)
        .bind(tag_id.0)
        .execute(&self.connection)
        .await
        {
            Ok(_) => {
                // if successful, save tags to entry, then return it
                Ok(())
            }
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e)
            }
        }
    }

    pub async fn get_entry(&self, entry_id: EntryId, user: User) -> Result<Entry, sqlx::Error> {
        match sqlx::query(
            "select * from entries where id = $1 and user_id = $2;
     ",
        )
        .bind(entry_id.0)
        .bind(user.id)
        .map(|row: PgRow| Entry {
            id: EntryId(row.get("id")),
            name: row.get("name"),
            url: row.get("url"),
            notes: row.get("notes"),
            tags: vec![],
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(entry) => Ok(self.query_tags_for_entry(entry).await),
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn update_entry(&self, entry: Entry, user: User) -> Result<Entry, sqlx::Error> {
        match sqlx::query(
            "UPDATE entries set name = $2, url = $3, notes = $4 where id = $1 and user_id = $5 RETURNING *;
     ",
        )
        .bind(entry.id.0)
        .bind(entry.name)
        .bind(entry.url)
        .bind(entry.notes)
        .bind(user.id)
        .map(|row: PgRow| Entry {
            id: EntryId(row.get("id")),
            name: row.get("name"),
            url: row.get("url"),
            notes: row.get("notes"),
            tags: vec![],
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(saved_entry) => {
                tracing::event!(
                    tracing::Level::INFO,
                    "Successfully saved entry {:?}",
                    saved_entry.clone()
                );

                match sqlx::query(
                    "delete from entries_tags where entry_id = $1;
             ",
                )
                .bind(saved_entry.clone().id.0)
                .execute(&self.connection)
                .await
                {
                    Ok(_) => {
                        tracing::event!(
                            tracing::Level::INFO,
                            "Cleared entry and tag relationships",
                        );
                    }
                    Err(e) => {
                        tracing::event!(
                            tracing::Level::ERROR,
                            "Problem clearing entry tag relationships {:?}",
                            e,
                        );
                        return Err(e);
                    }
                };

                for tag in entry.tags {
                    match self
                        .save_entry_tag(saved_entry.clone().id, tag.id.clone())
                        .await
                    {
                        Ok(_) => {
                            tracing::event!(
                                tracing::Level::INFO,
                                "Saved entry and tag relationship",
                            );
                        }
                        Err(e) => {
                            tracing::event!(
                                tracing::Level::ERROR,
                                "Could not save entry and tag relationship {:?}",
                                e
                            );
                            return Err(e);
                        }
                    };
                }
                Ok(self.query_tags_for_entry(saved_entry.clone()).await)
            }
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn update_entry_id_list(
        &self,
        entry: UpdatedEntry,
        user: User,
    ) -> Result<Entry, sqlx::Error> {
        match sqlx::query(
            "UPDATE entries set name = $2, url = $3, notes = $4 where id = $1 and user_id = $5 RETURNING *;
     ",
        )
        .bind(entry.id.0)
        .bind(entry.name)
        .bind(entry.url)
        .bind(entry.notes)
        .bind(user.id)
        .map(|row: PgRow| Entry {
            id: EntryId(row.get("id")),
            name: row.get("name"),
            url: row.get("url"),
            notes: row.get("notes"),
            tags: vec![],
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(saved_entry) => {
                tracing::event!(
                    tracing::Level::INFO,
                    "Successfully saved entry {:?}",
                    saved_entry.clone()
                );

                match sqlx::query(
                    "delete from entries_tags where entry_id = $1;
             ",
                )
                .bind(saved_entry.clone().id.0)
                .execute(&self.connection)
                .await
                {
                    Ok(_) => {
                        tracing::event!(
                            tracing::Level::INFO,
                            "Cleared entry and tag relationships",
                        );
                    }
                    Err(e) => {
                        tracing::event!(
                            tracing::Level::ERROR,
                            "Problem clearing entry tag relationships {:?}",
                            e,
                        );
                        return Err(e);
                    }
                };

                for tag_id in entry.tag_ids {
                    match self
                        .save_entry_tag(saved_entry.clone().id, tag_id.clone())
                        .await
                    {
                        Ok(_) => {
                            tracing::event!(
                                tracing::Level::INFO,
                                "Saved entry and tag relationship",
                            );
                        }
                        Err(e) => {
                            tracing::event!(
                                tracing::Level::ERROR,
                                "Could not save entry and tag relationship {:?}",
                                e
                            );
                            return Err(e);
                        }
                    };
                }
                Ok(self.query_tags_for_entry(saved_entry.clone()).await)
            }
            Err(e) => {
                tracing::event!(tracing::Level::ERROR, "{:?}", e);
                Err(e) //Err(Error::DatabaseQueryError) ??
            }
        }
    }

    pub async fn delete_entry(&self, entry_id: EntryId, user: User) -> Result<(), sqlx::Error> {
        // todo: guard against user id
        match sqlx::query("DELETE from entries_tags where entry_id = $1")
            .bind(entry_id.0)
            .execute(&self.connection)
            .await
        {
            Ok(_) => {
                match sqlx::query("DELETE from entries where id = $1 and user_id = $2;")
                    .bind(entry_id.0)
                    .bind(user.id)
                    .execute(&self.connection)
                    .await
                {
                    Ok(_) => Ok(()),
                    Err(e) => {
                        tracing::event!(
                            tracing::Level::ERROR,
                            "Error when deleting from entries: {:?}",
                            e
                        );
                        Err(e)
                    }
                }
            }
            Err(e) => {
                tracing::event!(
                    tracing::Level::ERROR,
                    "Error when deleting from entries_tags: {:?}",
                    e
                );
                Err(e)
            }
        }
    }

    pub async fn create_user(&self, new_user: NewUser) -> Result<User, sqlx::Error> {
        let Ok(hashed_password) = hash_password(new_user.cleartext_password) else {
            panic!("could not hash password")
        };
        match sqlx::query(
            "INSERT INTO users (username, password_hash) values ($1, $2)  returning *",
        )
        .bind(new_user.username)
        .bind(hashed_password)
        .map(|row: PgRow| User {
            id: row.get("id"),
            username: row.get("username"),
            password_hash: row.get("password_hash"),
        })
        .fetch_one(&self.connection)
        .await
        {
            Ok(user) => Ok(user),
            Err(e) => Err(e),
        }
    }

    // deletes all data associated with this user.
    pub async fn delte_all_user_data(&self, user_id: Uuid) -> Result<bool, sqlx::Error> {
        // delte entries_tags where the tag_id belongs to the user
        let _query_entries_tags_tag_id = sqlx::query(
            "
            DELETE FROM entries_tags WHERE tag_id IN (SELECT id FROM tags WHERE user_id = $1)
            ",
        )
        .bind(user_id)
        .execute(&self.connection)
        .await?;

        // delte entries_tags where the entry_id belongs to the user
        let _query_entries_tags_entry_id = sqlx::query(
            "
            DELETE FROM entries_tags WHERE entry_id IN (SELECT id FROM entries WHERE user_id = $1)
            ",
        )
        .bind(user_id)
        .execute(&self.connection)
        .await?;

        // delete tages belonging to the user
        let _query_tags = sqlx::query("DELETE from tags where user_id = $1")
            .bind(user_id)
            .execute(&self.connection)
            .await?;

        // delete entries belonging to the user
        let _query_entries = sqlx::query("DELETE from entries where user_id = $1")
            .bind(user_id)
            .execute(&self.connection)
            .await?;

        // delete the user itself.
        let _query_users = sqlx::query("DELETE from users where id = $1")
            .bind(user_id)
            .execute(&self.connection)
            .await?;
        Ok(true)
    }
}

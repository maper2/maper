use askama::Template;
use axum::{
    http::StatusCode,
    response::{Html, IntoResponse, Response},
};

use crate::{
    types::{entry::Entry, tag::Tag, user::User},
    views::entry::EntriesFilterParams,
};

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate {
    pub user: Option<User>,
}

#[derive(Template, Debug)]
#[template(path = "authentication/signup.html")]
pub struct SignupTemplate {
    pub user: Option<User>,
    pub error: Option<String>,
}

#[derive(Template)]
#[template(path = "authentication/login.html")]
pub struct LoginTemplate {
    pub user: Option<User>,
    pub errors: Vec<String>,
}

#[derive(Template)]
#[template(path = "tag/tag_select.html")]
pub struct TagSelectTemplate {
    pub tags: Vec<Tag>,
    pub user: Option<User>,
}

#[derive(Template)]
#[template(path = "tag/tag_edit.html")]
pub struct TagEditTemplate {
    pub tag: Tag,
    pub user: Option<User>,
}

#[derive(Template)]
#[template(path = "entries/entry_list.html")]
pub struct EntriesListTemplate {
    pub tags: Vec<Tag>,
    pub entries: Vec<Entry>,
    pub filter_params: EntriesFilterParams,
    pub user: Option<User>,
}
#[derive(Template)]
#[template(path = "entries/entry_edit.html")]
pub struct EntryEditTemplate {
    pub tags: Vec<Tag>,
    pub entry: Entry,
    pub user: Option<User>,
}

pub struct HtmlTemplate<T>(pub T);

impl<T> IntoResponse for HtmlTemplate<T>
where
    T: Template,
{
    fn into_response(self) -> Response {
        match self.0.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {}", err),
            )
                .into_response(),
        }
    }
}

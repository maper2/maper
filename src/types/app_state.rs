use crate::{config::Config, store::Store};

#[derive(Clone, Debug)]
pub struct AppState {
    pub store: Store,
    pub config: Config,
}

// this is necessary because AppState needs to be send & sync for it to be usable in FromRequestParts
// which in turn is needed for the custom Claims
unsafe impl Send for AppState {}
unsafe impl Sync for AppState {}

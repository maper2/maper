use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::tag::{Tag, TagId};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct EntryId(pub Uuid);

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct Entry {
    pub id: EntryId,
    pub name: String,
    pub url: String,
    pub notes: Option<String>,
    #[serde(default = "default_tags")]
    pub tags: Vec<Tag>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct NewEntry {
    pub name: String,
    pub url: String,
    pub notes: Option<String>,
    #[serde(default = "default_tag_ids")]
    pub tag_ids: Vec<TagId>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct UpdatedEntry {
    pub id: EntryId,
    pub name: String,
    pub url: String,
    pub notes: Option<String>,
    #[serde(default = "default_tag_ids")]
    pub tag_ids: Vec<TagId>,
}

fn default_tags() -> Vec<Tag> {
    vec![]
}
fn default_tag_ids() -> Vec<TagId> {
    vec![]
}

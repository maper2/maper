use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct Tag {
    pub id: TagId,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct TagId(pub Uuid);

#[derive(Deserialize, Serialize, Debug)]
pub struct NewTag {
    pub name: String,
}

use axum_login::AuthUser;
use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use uuid::Uuid;

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, FromRow)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub password_hash: String,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, FromRow)]
pub struct FilteredUser {
    pub id: Uuid,
    pub username: String,
}

#[derive(Deserialize, Serialize)]
pub struct NewUser {
    pub username: String,
    pub cleartext_password: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct NewUserFormData {
    pub username: String,
    pub password: String,
    pub password_repeat: String,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct LoginFormData {
    pub username: String,
    pub password: String,
}

impl AuthUser for User {
    type Id = Uuid;

    fn id(&self) -> Uuid {
        self.id
    }

    fn session_auth_hash(&self) -> &[u8] {
        self.password_hash.as_bytes()
    }
}

pub fn filter_user_record(user: &User) -> FilteredUser {
    FilteredUser {
        id: user.id,
        username: user.username.clone(),
    }
}

use axum::{
    extract::{Query, State},
    response::{IntoResponse, Redirect},
};
use axum_extra::extract::Form;
use serde::Deserialize;

use crate::{
    authentication::{
        password_hash::verify_password_hash, password_quality::check_password_quality,
    },
    store::AuthSession,
    templates::{HtmlTemplate, LoginTemplate, SignupTemplate},
    types::user::{LoginFormData, NewUser, NewUserFormData, User},
    AppState,
};

#[derive(Deserialize)]
pub struct Error {
    pub err: Option<String>,
}

pub async fn signup_form(auth: AuthSession, error: Query<Error>) -> impl IntoResponse {
    let template = SignupTemplate {
        user: auth.user,
        error: error.0.err,
    };
    HtmlTemplate(template)
}

pub async fn handle_user_signup(state: &AppState, new_user: NewUser) -> Result<User, sqlx::Error> {
    match state.store.create_user(new_user).await {
        Ok(user) => {
            tracing::event!(tracing::Level::INFO, "Saved new user {:?}", user);
            Ok(user)
        }
        Err(e) => {
            tracing::event!(tracing::Level::ERROR, "Error when saving new user {:?}", e);
            Err(e)
        }
    }
}

pub async fn signup_post(
    State(state): State<AppState>,
    Form(new_user_form_data): Form<NewUserFormData>,
) -> Redirect {
    if new_user_form_data.password == new_user_form_data.password_repeat {
        let errors = check_password_quality(new_user_form_data.password.clone());

        if errors.is_empty() {
            let new_user = NewUser {
                username: new_user_form_data.username,
                cleartext_password: new_user_form_data.password,
            };

            if (handle_user_signup(&state, new_user).await).is_err() {
                return Redirect::to(
                    "/signup?err=Could not create the user, maybe try another username?",
                );
            };
        } else {
            tracing::event!(
                tracing::Level::ERROR,
                "The password is not good enough: {:?}",
                errors
            );

            let err_message = format!("The password is not good enough: {:}", errors.join(", "));
            return Redirect::to(format!("/signup?err={}", err_message).as_str());

            //return Redirect::to("/signup?err=Could not create new user. Perhaps try a different username?");
        }
    } else {
        return Redirect::to("/signup?err=The passwords did not match.");
    }

    Redirect::to("/login") // login?
}

pub async fn login_form(
    auth: AuthSession,
    State(_state): State<AppState>,
    error: Query<Error>,
) -> impl IntoResponse {
    let e: Error = error.0;
    match e.err {
        Some(err) => {
            let template = LoginTemplate {
                user: auth.user,
                errors: vec![err.clone()],
            };
            HtmlTemplate(template)
        }
        None => {
            let template = LoginTemplate {
                user: auth.user,
                errors: vec![],
            };
            HtmlTemplate(template)
        }
    }
}

pub async fn login_handler(
    mut auth: AuthSession,
    State(state): State<AppState>,
    Form(login_form_data): Form<LoginFormData>,
) -> Redirect {
    tracing::event!(tracing::Level::INFO, "login_handler");

    let Ok(user): Result<User, sqlx::Error> =
        sqlx::query_as("select * from users where username = $1 limit 1")
            .bind(login_form_data.username)
            .fetch_one(&state.store.connection)
            .await
    else {
        tracing::event!(tracing::Level::ERROR, "Could not find user by username");

        return Redirect::to("login?err=Could not log in with provided credentials.");
    };

    if verify_password_hash(login_form_data.password, user.password_hash.clone()) {
        tracing::event!(tracing::Level::WARN, "Logging in {}", &user.username);

        auth.login(&user).await.unwrap();
        tracing::event!(tracing::Level::WARN, "logged in as {:?}", user);
        Redirect::to("entries")
    } else {
        tracing::event!(tracing::Level::ERROR, "could not login!{:?}", user);
        Redirect::to("login?err=Could not log in with provided credentials.")
    }
}

pub async fn logout_handler(mut auth: AuthSession) -> impl IntoResponse {
    match &auth.user {
        Some(user) => {
            tracing::event!(tracing::Level::INFO, "Logging out User {:?}", user);

            let _ = auth.logout();
        }
        None => {
            tracing::event!(tracing::Level::INFO, "No user was logged in",);
        }
    }

    Redirect::to("/")
}

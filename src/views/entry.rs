use std::str::FromStr;

use crate::{
    store::AuthSession,
    templates::{EntriesListTemplate, EntryEditTemplate, HtmlTemplate},
    types::{
        app_state::AppState,
        entry::{EntryId, NewEntry, UpdatedEntry},
    },
};
use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
    response::{IntoResponse, Redirect},
};
use axum_extra::extract::Form;
use serde::Deserialize;
use uuid::Uuid;

#[derive(Debug, Deserialize, Clone)]
pub struct EntriesFilterParamsRaw {
    pub tag_id: Option<String>,
    pub search: Option<String>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct EntriesFilterParams {
    pub tag_id: Option<Uuid>,
    pub search: Option<String>,
    pub limit: Option<i32>,
    pub offset: Option<i32>,
}

impl EntriesFilterParams {
    pub fn from_raw(p: EntriesFilterParamsRaw) -> Option<Self> {
        let tag_id: Option<Uuid> = match p.tag_id {
            None => None,
            Some(s) => match s.is_empty() {
                true => None,
                false => Some(
                    Uuid::from_str(s.as_str())
                        .expect("that s could be parsed to a uuid at this point."),
                ),
            },
        };

        Some(EntriesFilterParams {
            search: p.search,
            tag_id,
            limit: None,
            offset: None,
        })
    }
}

pub async fn display_entries(
    auth: AuthSession,
    State(state): State<AppState>,
    Query(params_raw): Query<EntriesFilterParamsRaw>,
) -> impl IntoResponse {
    // todo: use the tag_id from the params to filter the entries in the store

    let params = EntriesFilterParams::from_raw(params_raw).unwrap();

    let Ok(entries) = state
        .store
        .get_entries(
            None,
            0,
            params.search.clone(),
            params.tag_id,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    else {
        panic!("some error for entries");
    };

    let Ok(tags) = state
        .store
        .get_tags(
            None,
            0,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    else {
        panic!("some error for tags");
    };

    let template = EntriesListTemplate {
        user: auth.user,
        entries,
        tags,
        filter_params: params,
    };
    HtmlTemplate(template)
}

pub async fn create_new_entry(
    auth: AuthSession,
    State(state): State<AppState>,
    Form(new_entry): Form<NewEntry>,
) -> impl IntoResponse {
    match state
        .store
        .save_new_entry(
            new_entry,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(entry) => {
            tracing::event!(tracing::Level::INFO, "Saved new entry {:?}", entry);
            display_entries(
                auth,
                State(state),
                Query(EntriesFilterParamsRaw {
                    tag_id: None,
                    search: None,
                }),
            )
            .await
        }
        Err(e) => {
            tracing::event!(tracing::Level::ERROR, "Error saving entry {:#?}", e);
            panic!("Error saving entry {:?}", e)
        }
    }

    // invoke another view function instead of redirecting
    // this could also be used to display error states.
}

pub async fn update_entry_form(
    auth: AuthSession,
    State(state): State<AppState>,
    Path(entry_id): Path<EntryId>,
) -> Result<impl IntoResponse, StatusCode> {
    let tags = state
        .store
        .get_tags(
            None,
            0,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
        .ok()
        .unwrap_or(vec![]);

    match state
        .store
        .get_entry(
            entry_id,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(entry) => {
            let template = EntryEditTemplate {
                tags,
                entry,
                user: auth.user,
            };
            Ok(HtmlTemplate(template))
        }
        Err(e) => {
            tracing::event!(
                tracing::Level::ERROR,
                "Error when getting tag by id {:?}",
                e
            );
            //return Redirect::temporary("/tags")
            Err(StatusCode::NOT_FOUND)
        }
    }
}

pub async fn update_entry(
    auth: AuthSession,
    State(state): State<AppState>,
    Form(entry): Form<UpdatedEntry>,
) -> impl IntoResponse {
    match state
        .store
        .update_entry_id_list(
            entry,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(entry) => tracing::event!(
            tracing::Level::INFO,
            "Updated entry as seen from view {:?}",
            entry
        ),
        Err(e) => tracing::event!(tracing::Level::WARN, "Error when updating entry {:?}", e),
    };
    // permanent sets method to get.
    Redirect::to("/entries")
}

pub async fn delete_entry(
    auth: AuthSession,
    State(state): State<AppState>,
    Path(entry_id): Path<EntryId>,
) -> impl IntoResponse {
    match state
        .store
        .delete_entry(
            entry_id,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(_) => {}
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when deleting entry {:?}", e)
        }
    };

    display_entries(
        auth,
        State(state),
        Query(EntriesFilterParamsRaw {
            tag_id: None,
            search: None,
        }),
    )
    .await
}

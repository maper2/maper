use axum::{extract::State, response::IntoResponse};

use crate::{
    store::AuthSession,
    templates::{HtmlTemplate, IndexTemplate},
    AppState,
};

pub async fn index(State(_state): State<AppState>, auth: AuthSession) -> impl IntoResponse {
    let template = IndexTemplate { user: auth.user };
    HtmlTemplate(template)
}

use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::{IntoResponse, Redirect},
};
use axum_extra::extract::Form;

use crate::{
    templates::{HtmlTemplate, TagEditTemplate, TagSelectTemplate},
    types::tag::{NewTag, Tag, TagId},
    AppState, store::AuthSession,
};

pub async fn tag_select(auth: AuthSession, State(state): State<AppState>) -> impl IntoResponse {
    let Ok(tags) = state
        .store
        .get_tags(
            None,
            0,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    else {
        panic!("some error");
    };

    let template = TagSelectTemplate {
        tags,
        user: auth.user,
    };
    HtmlTemplate(template)
}

pub async fn create_new_tag(
    auth: AuthSession,
    State(state): State<AppState>,
    Form(new_tag): Form<NewTag>,
) -> impl IntoResponse {
    match state
        .store
        .save_new_tag(
            new_tag,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(tag) => tracing::event!(tracing::Level::WARN, "Saved new tag {:?}", tag),
        Err(e) => tracing::event!(tracing::Level::WARN, "Error when saving new tag {:?}", e),
    };

    tag_select(auth, State(state)).await
}

pub async fn update_tag_form(
    auth: AuthSession,
    State(state): State<AppState>,
    Path(tag_id): Path<TagId>,
) -> Result<impl IntoResponse, StatusCode> {
    match state
        .store
        .get_tag(
            tag_id,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(tag) => {
            let template = TagEditTemplate {
                tag,
                user: auth.user,
            };
            Ok(HtmlTemplate(template))
        }
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when getting tag by id {:?}", e);
            //return Redirect::temporary("/tags")
            Err(StatusCode::NOT_FOUND)
        }
    }
}

pub async fn update_tag(
    auth: AuthSession,
    State(state): State<AppState>,
    Form(tag): Form<Tag>,
) -> impl IntoResponse {
    match state
        .store
        .update_tag(
            tag,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(tag) => tracing::event!(tracing::Level::WARN, "Updated tag {:?}", tag),
        Err(e) => tracing::event!(tracing::Level::WARN, "Error when updating tag {:?}", e),
    };
    Redirect::temporary("/tags")
}

pub async fn delete_tag(
    auth: AuthSession,
    State(state): State<AppState>,
    Path(tag_id): Path<TagId>,
) -> impl IntoResponse {
    match state
        .store
        .delete_tag(
            tag_id,
            auth.user
                .clone()
                .expect("Should be a valid user, because of auth middleware"),
        )
        .await
    {
        Ok(_) => {}
        Err(e) => {
            tracing::event!(tracing::Level::WARN, "Error when deleting tag {:?}", e)
        }
    };

    tag_select(auth, State(state)).await
}
